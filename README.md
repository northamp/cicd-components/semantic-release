# Semantic-Release CI/CD component

CI/CD Component that runs [`semantic-release`](https://github.com/semantic-release/semantic-release). All it does for now is install and run it, though it may come with a custom image later on.

You will have to provide your project with a [configuration file](https://semantic-release.gitbook.io/semantic-release/usage/configuration) (refer to this project's own file for a simple reference).

It is also necessary to set the Gitlab CI/CD Variable `GITLAB_TOKEN` to an access token with API and Repository rights ([docs](https://semantic-release.gitbook.io/semantic-release/usage/ci-configuration)).

## Usage

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - component: ${CI_SERVER_HOST}/northamp/cicd-components/semantic-release/semantic-release@~latest
```

Replace `~latest` to a specific tag if you desire.

It's also possible to use rules and inputs to decide when to run the job, and define some parameters.

```yaml
include:
  - component: ${CI_SERVER_HOST}/northamp/cicd-components/semantic-release/semantic-release@~latest
    inputs:
      stage: release
    rules:
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH == "beta"
```

### Inputs

| Input          | Default value    | Description                                                     |
| -------------- | ---------------- | --------------------------------------------------------------- |
| `stage`        | `release`        | The stage where you want the job to be added                    |
| `image_prefix` | `node`           | Image that should be used by the job in format `registry/image` |
| `image_tag`    | `current-alpine` | Tag of the image, will be appended to `image_prefix`            |
| `dry_run`      | `""`             | Set to `true` to run semantic-release in dry-run mode           |
